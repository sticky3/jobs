<img src="https://gitlab.com/sticky3/jobs/-/raw/main/we_are_hiring.jpeg" width="500px" />

# Senior iOS Developer (Swift)

## About Us:
* Sticky aims to be the leading worldwide mobile-native token marketplace.
* Creators & collectors on Sticky get the fun of NFTs and other non-currency tokens in a fun & inviting app, with the safety of the App Store, without the hassle of setting up crypto wallets.
* Launched in early 2022 by experienced founders, it has received early-stage backing from top VC investors and is now building an all-star, remote-first team.
* iOS: https://apps.apple.com/us/app/nft-marketplace-maker-sticky/id1497044357
* Web: https://getstic.ky
 
## This job:
* You will be responsible for planning and building features for the Sticky app on iOS, using Swift, alongside our Co-CTO, who is an experienced iOS developer.
* This means you have the benefits both of having a very strong collaborator,  and of having the opportunity to grow (the more you can take full responsibility for iOS, and free-up his time to focus on other areas, the better)
* Key areas of focus within Sticky are:
* Implementing interfaces and consuming data from REST APIs 
* In-App-Purchases, NFT Transactions & Logs
* Optimizing performance (high volume of images)
* Usability
* Fixing bugs

## You need to have:
* Proficient iOS development skills - subject to technical test.
* 3+ years of experience developing in Swift for iOS
* Coding & version control best practices
* Basic English skills to read documentation & communicate with the rest of the team.
* Interest in working on the Sticky app
 
## If you have these too that's nice:
* Entrepreneurial experience (e.g. a startup or side project of your own)
* Early-stage startup experience 
* Interest or experience with blockchain, cryptocurrencies, NFTs
* Strong academic credentials (e.g. top university, degree in Computer Science / Engineering)
* Practical experience with automated testing development
* Knowledge of other mobile development languages & platforms: e.g. ObjC, React Native, Flutter, Kotlin, etc.
* Knowledge of other languages and platforms we use, such as: Node.js, SQL, Solidity, Python, Elastic, etc.
 
## We offer:
* Blood, toil, tears and sweat.
* Generous salary and stock options.
* The opportunity to build and grow with a cutting-edge, VC-backed global marketplace from the start, as one of its first 10 developers
* An entrepreneurial, pragmatic, high performance, transparent and friendly culture.

## About Us:
* Sticky aims to be the leading worldwide mobile-native token marketplace.
* Creators & collectors on Sticky get the fun of NFTs and other non-currency tokens in a fun & inviting app, with the safety of the App Store, without the hassle of setting up crypto wallets.
* Sticky has a private ledger and is in the process of integrating with Ethereum, Polygon and Solana. 
* Launched in early 2022 by experienced founders, it has received early-stage backing from top VC investors and is now building an all-star, remote-first team.
* iOS: https://apps.apple.com/us/app/nft-marketplace-maker-sticky/id1497044357
* Web: https://getstic.ky

## Place:
* Wherever you want (fully remote)

## How to apply
If you believe that you are a good fit for this job, please contact us and send your updated resume, LinkedIn profile and salary expectation to talent@getstic.ky
