<img src="https://gitlab.com/sticky3/jobs/-/raw/main/we_are_hiring.jpeg" width="500px" />

# Senior Node JS developer

## About Us:
* Sticky is the leading mobile-native token marketplace, and we are now beginning to build our platform on the web.
* Creators & collectors on Sticky get the fun of NFTs and other non-currency tokens in a fun & inviting app without the hassle of setting up crypto wallets.
* Launched in early 2022 by experienced founders, it has received early-stage backing from top VC investors and is now building an all-star, remote-first team.
* iOS: https://apps.apple.com/us/app/nft-marketplace-maker-sticky/id1497044357
* Web: https://getstic.ky


## This job:
* You will be responsible for leading the backend development of our platform
* Lead architecture design with input from other tech leaders
* This is a unique opportunity to lead backend & architecture for a VC-backed global marketplace.

### Key priorities include:
* Further developing the features and performance of our marketplace
* Integrating our systems with public blockchains (Ethereum, Polygon, Solana)
* Ensuring scalability & security.

## You need to have:
* 5+ years of experience with system development.
* 2+ years of experience with Node.js.
* Familiarity with SQL databases (e.g. MariaDB, MySQL, PostgreSQL)
* Familiarity with Google Cloud Platform and AWS.
* Practical experience with automated testing development
* Architecture, security, coding, version control best practices
* Practical experience with automated testing development

## If you have these too that's nice:
* Interest or experience with blockchain, cryptocurrencies, NFTs
* Entrepreneurial experience (e.g. a startup or side project of your own)
* Early-stage startup or Fintech experience 
* Strong academic credentials (e.g. top university, degree in Computer Science / Engineering)
* Knowledge of other languages and platforms we use, such as: Python, Elastic.
* DevOps experience

## We offer:
* Blood, toil, tears and sweat.
* Generous salary and stock options.
* The opportunity to build and grow with a cutting-edge global marketplace from the start, as one of its first 10 developers

## Place:
* Wherever you want (fully remote)

## How to apply
If you believe that you are a good fit for this job, please contact us and send your updated resume, LinkedIn profile and salary expectation to talent@getstic.ky

