<img src="https://gitlab.com/sticky3/jobs/-/raw/main/we_are_hiring.jpeg" width="500px" />

# Senior react native developer

## About Us:
* Sticky aims to be the leading worldwide mobile-native token marketplace.
* Creators & collectors on Sticky get the fun of NFTs and other non-currency tokens in a fun & inviting app, with the safety of the App Store, without the hassle of setting up crypto wallets.
* Launched in early 2022 by experienced founders, it has received early-stage backing from top VC investors and is now building an all-star, remote-first team.
* iOS: https://apps.apple.com/us/app/nft-marketplace-maker-sticky/id1497044357
* Web: https://getstic.ky

## This job:
* You will be responsible for leading implementation of the Sticky app for Android, using React Native.
* Initially working to catch up with iOS version, then working alongside our Co-CTO, who is an experienced iOS developer, to plan mobile features for iOS and Android together.
* Key areas of focus within Sticky are:
* Implementing interfaces and consuming data from REST APIs 
* In-App-Purchases, NFT Transactions & Logs
* Optimizing performance (high volume of images)
* Usability
* Fixing bugs

## You need to have:
* Proficient React Native development skills - subject to technical test.
* 3+ years of experience developing for React Native
* Coding & version control best practices
* Basic English skills to read documentation & communicate with the rest of the team.
* Interest in working on the Sticky app

## If you have these too that's nice:
* Entrepreneurial experience (e.g. a startup or side project of your own)
* Early-stage startup experience 
* Interest or experience with blockchain, cryptocurrencies, NFTs
* Strong academic credentials (e.g. top university, degree in Computer Science / Engineering)
* Practical experience with automated testing development
* Knowledge of mobile development languages & platforms: e.g. Swift, Flutter, etc.
* Knowledge of other languages and technologies we use, such as: Node.js, SQL, Solidity, Python, Elastic, etc.
 
## We offer:
* Blood, toil, tears and sweat.
* Generous salary and stock options.
* The opportunity to build and grow with a cutting-edge VC-backed global marketplace from the start, as one of its first 10 developers
* An entrepreneurial, pragmatic, high performance, transparent and friendly culture.


## Place:
* Wherever you want (fully remote)

## How to apply
If you believe that you are a good fit for this job, please contact us and send your updated resume, LinkedIn profile and salary expectation to talent@getstic.ky
