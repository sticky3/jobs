<img src="https://gitlab.com/sticky3/jobs/-/raw/main/we_are_hiring.jpeg" width="500px" />

# Lead blockchain developer

## About Us:
* Sticky aims to be the leading worldwide mobile-native token marketplace.
* Creators & collectors on Sticky get the fun of NFTs and other non-currency tokens in a fun & inviting app, with the safety of the App Store, without the hassle of setting up crypto wallets.
* Sticky has a private ledger and is in the process of integrating with Ethereum, Polygon and Solana. 
* Launched in early 2022 by experienced founders, it has received early-stage backing from top VC investors and is now building an all-star, remote-first team.
* iOS: https://apps.apple.com/us/app/nft-marketplace-maker-sticky/id1497044357
* Web: https://getstic.ky
 
## This job:
* You will be responsible for leading all aspects of our technology that involves blockchains, both public (Ethereum, Polygon, Solana) and our private ledger.
* This is a unique opportunity to lead blockchain technology for a VC-backed global marketplace.

### Key priorities include:
* Integrating purchases in our app (USD) with transactions on public blockchains (Ethereum, Polygon, Solana)
* Bridging our ledger with public blockchains (e.g. letting users export/import NFTs from one to the other)
* Further design, development of our ledger/blockchain and of the NFT standards
* Ensuring performance & security.

## You need to have:
* Proficient blockchain development skills - subject to technical test
* 5+ years of experience with Blockchain development (e.g. Solidity, Hyperledger, R3 Corda, Bluemix, Rubix)
* Deep knowledge of blockchain, cryptocurrencies, system architecture, data structure and algorithms.
* Security, coding & version control best practices
* Basic English skills to read documentation & communicate with the rest of the team.
* Interest in working on the Sticky platform.

## If you have these too that's nice:
* Entrepreneurial experience (e.g. a startup or side project of your own)
* Early-stage startup experience 
* Strong academic credentials (e.g. top university, degree in Computer Science / Engineering)
* Practical experience with automated testing development
 
## We offer:
* Blood, toil, tears and sweat.
* Generous salary and stock options.
* The opportunity to build and grow with a cutting-edge VC-backed global marketplace from the start, as one of its first 10 developers
* An entrepreneurial, pragmatic, high performance, transparent and friendly culture.
 
## Place:
* Wherever you want (fully remote)

## How to apply
If you believe that you are a good fit for this job, please contact us and send your updated resume, LinkedIn profile and salary expectation to talent@getstic.ky
