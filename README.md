<img src="https://gitlab.com/sticky3/jobs/-/raw/main/we_are_hiring.jpeg" width="500px" />

# You are welcome!
On this page, you can check out all our open opportunities.
If you are interested in any of these positions, just send us an email (talent@getstic.ky) with your updated CV, LinkedIn link and salary expectation.

# About us
* Sticky is the leading mobile-native token marketplace, and we are now beginning to build our platform on the web.
* Creators & collectors on Sticky get the fun of NFTs and other non-currency tokens in a fun & inviting app without the hassle of setting up crypto wallets.
* Launched in early 2022 by experienced founders, it has received early-stage backing from top VC investors and is now building an all-star, remote-first team.
* iOS: https://apps.apple.com/us/app/nft-marketplace-maker-sticky/id1497044357
* Web: https://getstic.ky

# Opportunities

- [Lead blockchain developer](https://gitlab.com/sticky3/jobs/-/blob/main/lead_blockchain_developer.md)
- [Senior Android developer](https://gitlab.com/sticky3/jobs/-/blob/main/senior_android_developer.md)
- [Senior iOS developer](https://gitlab.com/sticky3/jobs/-/blob/main/senior_ios_developer.md)
- [Senior Frontend developer](https://gitlab.com/sticky3/jobs/-/blob/main/senior_frontend_developer.md)
- [Senior Backend developer](https://gitlab.com/sticky3/jobs/-/blob/main/senior_nodejs.md)
