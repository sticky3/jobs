<img src="https://gitlab.com/sticky3/jobs/-/raw/main/we_are_hiring.jpeg" width="500px" />

# Senior Cyber Security Analyst

## About Us:
* Sticky aims to be the leading worldwide mobile-native token marketplace.
* Creators & collectors on Sticky get the fun of NFTs and other non-currency tokens in a fun & inviting app, with the safety of the App Store, without the hassle of setting up crypto wallets.
* Launched in early 2022 by experienced founders, it has received early-stage backing from top VC investors and is now building an all-star, remote-first team.
* iOS: https://apps.apple.com/us/app/nft-marketplace-maker-sticky/id1497044357
* Web: https://getstic.ky

## This job:
You will be responsible for search, identify and help to fix security problems on:
- our apps (android & iOS);
- our website;
- our API's (Backend & Blockchain);
- our libs and plugins;
- the internal systems and process of the company.

You'll be responsible too to:
- controlling accounts and access profiles of our services and employees;
- development of IT Disaster Recovery Plans;
- Treatment of vulnerabilities exposed to the internet and internal vulnerabilities;
- Predictive analysis in security system on a periodic basis;
- help to detect and prevent frauds on our transactions;
- garantee that our systems are running and developed with highest level of security, following the most advanced practices of the market;
- ensure that external services that are being used and consumed by our systems are being done safely;


## You need to have:
* Knowledge of security applied to network, operational systems, databases and systems;
* Knowledge of Firewalls, IPS/IDS and Proxy;
* Knowledge on cloud (AWS or GCP);
* Almost 3 years of experience working as security analyst;

## If you have these too that's nice:
* Experience working as security analyst of a fintech, bank or crypto platform;
* Knowledge in CI/CD process (Jenkins, Gitlab CI or any related tool).
* MBA or a specialization focused on security;
* Entrepreneurial experience (e.g. a startup or side project of your own)
* Early-stage startup experience 
* Interest or experience with blockchain, cryptocurrencies, NFTs
* Strong academic credentials (e.g. top university, degree in Computer Science / Engineering)
* Practical experience with automated testing development
* Knowledge of other mobile development languages & platforms: e.g. Swift, React Native, Flutter, etc.
* Knowledge of other languages and technologies we use, such as: Node.js, SQL, Solidity, Python, Elastic, etc.
 
## We offer:
* Blood, toil, tears and sweat.
* Generous salary and stock options.
* The opportunity to build and grow with a cutting-edge VC-backed global marketplace from the start, as one of its first 10 developers
* An entrepreneurial, pragmatic, high performance, transparent and friendly culture.


## Place:
* Wherever you want (fully remote)

## How to apply
If you believe that you are a good fit for this job, please contact us and send your updated resume, LinkedIn profile and salary expectation to talent@getstic.ky
